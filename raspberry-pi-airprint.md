# Creating a Raspberry Pi Airprint Server with a networked Brother HL-2270DW Printer

Start with a Raspberry Pi that has a clean install of Pi OS.  I'm using a
[Raspberry Pi Model B](https://en.wikipedia.org/wiki/Raspberry_Pi).

## [Post Raspberry Pi OS install configuration](https://www.raspberrypi.org/documentation/configuration/raspi-config.md)

Setup locale, hostname, timezone, and expand filesystem.
```
sudo raspi-config
sudo systemctl reboot
```

## [Update and Upgrade](https://www.raspberrypi.org/documentation/raspbian/updating.md)

Upgrade package database and upgrade any outdated packages.
```
sudo apt-get update
sudo apt-get upgrade
sudo systemctl reboot
```

## [Install CUPS](https://pimylifeup.com/raspberry-pi-print-server/)

```
sudo apt-get install cups
sudo usermod -a -G lpadmin pi
sudo cupsctl --remote-any
sudo systemctl reboot
```
## Install [brlaser](https://github.com/pdewacht/brlaser)

Verify the packaged brlaser driver version number.

```
apt-cache show printer-driver-brlaser
```

As of 2021.02 the Pi OS packaged driver was outdated (version 4) and it was
necessary to compile it.  The current code contained support for new printers
and an important bug fix for my printer.

Either add the package:

```
sudo agt-get install printer-driver-brlaser
```

Or compile the source code:

```
sudo apt-get install libcups2-dev libcupsimage2-dev cmake
git clone https://github.com/pdewacht/brlaser
cd brlaser
cmake .
make
sudo make install
sudo systemctl reboot
```

## Browse to CUPS admin webpage and add printer

Browse to `http://my-raspberry-pi-ipaddress:631` and add printer; use login credentials for pi.
